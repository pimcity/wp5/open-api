# EasyPIMS Open APIs

Open APIs are a key part of SDKs as they permit to seamlessly integrate PIMCity components for enabling their interoperability and re-usability. They represent a logic substratum of endpoints which will be designed and implemented uniformly across all components, so that the same security, privacy and scalability requirements will be met overall the deployment.
In this deliverable D5.1 we report the principles and requirements that drove us in the design of the Open APIs for all components building PIMCity’s PDK and EasyPIMS. In D5.1, we also present the standards we adopted for the design and the tools chosen to accelerate the implementation. 

In this repository, for each component designed in WP2, WP3 and WP4, we provide the design and implementation of its APIs.
