openapi: 3.0.0
info:
  title: Data Trading Engine
  version: '1.0'
  contact:
    name: Dani Fernandez
    email: daniel@wibson.io
    url: 'https://wibson.io'
  license:
    name: Open Source
  description: |-
    Data Trading Engine API.

    Its primary objective is to execute all transactions within the platform to exchange data for value in a secure,
    transparent and fair-for-all way. Moreover, its key requirement is to be fully GDPR compliant. That is, it must
    be able to receive Data Offers from Data Buyers and fulfil them with users from the PIMCity platform that not only
    fit within the target data sought, but also that have proactively consented to share that data with that company
    for a specific purpose. 
servers:
  - url: 'https://easypims.pimcity-h2020.eu/dte-api'
paths:
  /health:
    get:
      summary: Checks if the server is running
      responses:
        '200':
          description: Server is up and running
        default:
          description: Something went wrong
      security: []   # No security
  /accounts/credits:
    get:
      description: It responds with the number of credits that the user has.
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  credits:
                    type: number
                    description: The number of credits
        '401':
          description: Unauthorized
      security:
        - BearerAuth: []
    post:
      description: It adds credits to the Data Buyer’s account.
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                credits:
                  type: number
                  description: The number of credits
      responses:
        '204':
          description: OK
        '401':
          description: Unauthorized
      security:
        - BearerAuth: []
  /market/price:
    get:
      description: It responds with the price for a piece of data for a specific audience.
      parameters:
        - in: query
          name: dataType
          schema:
            type: string
          description: The type of data.
        - in: query
          name: audience
          schema:
            type: string
          description: The audience corresponding with the price.
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  pricePerIndividual:
                    type: number
                    description: Price per individual.
        '401':
          description: Unauthorized
      security:
        - BearerAuth: []
  /market/individuals/data-offers:
    get:
      description: It returns a list of all the "Data Offers" accepted by an individual.
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      description: The Data Offer ID.
                    audience:
                      type: string
                      description: The audience being sought.
                    dataType:
                      type: string
                      description: The type of data shared.
                    companyId:
                      type: string
                      description: The company ID.
                    purpose:
                      type: string
                      description: The purpose used to process the data.
                    termsAndConditions:
                      type: string
                      description: The terms and conditions for the Data Offer.
                    offerBudget:
                      type: number
                      description: The budget for the offer.
        '401':
          description: Unauthorized
      security:
        - BearerAuth: []
  /market/individuals/transactions:
    post:
      description: It logs a transaction for a user
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                userId:
                  type: string
                  description: The ID of the user within PIMCity.
                id:
                  type: string
                  description: The ID of the transaction to be logged.
                description:
                  type: string
                  description: A description for the transaction to be logged.
                credits:
                  type: number
                  description: The amount of credits the user earned for this transaction.
      responses:
        '204':
          description: OK
        '401':
          description: Unauthorized
      security:
        - ApiKeyAuth: []
  /market/individuals/transactions/{userId}:
    get:
      description: It returns a list of all the transactions logged for a user
      parameters:
        - in: path
          name: userId
          schema:
            type: string
          required: true
          description: ID of the user to get
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      description: The ID of the transaction to be logged.
                    description:
                      type: string
                      description: A description for the transaction to be logged.
                    credits:
                      type: number
                      description: The amount of credits the user earned for this transaction.
        '401':
          description: Unauthorized
      security:
        - ApiKeyAuth: []
  /market/buyers/data-offers:
    get:
      description: It returns a list of all the "Data Offers" placed by the Data Buyer.
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  type: object
                  properties:
                    id:
                      type: string
                      description: The Data Offer ID.
                    audience:
                      type: string
                      description: The audience being sought.
                    dataType:
                      type: string
                      description: The type of data shared.
                    companyId:
                      type: string
                      description: The company ID.
                    purpose:
                      type: string
                      description: The purpose used to process the data.
                    termsAndConditions:
                      type: string
                      description: The terms and conditions for the Data Offer.
                    offerBudget:
                      type: number
                      description: The budget for the offer.
        '401':
          description: Unauthorized
      security:
        - BearerAuth: []
    post:
      description: It places a "Data Offer" on behalf of a Data Buyer, specifying the target audience, type(s) of data, buyer info, the terms and conditions and the offer budget.
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                audience:
                  type: string
                  description: The audience being sought.
                dataType:
                  type: string
                  description: The type of data shared.
                companyId:
                  type: string
                  description: The company ID.
                purpose:
                  type: string
                  description: The purpose used to process the data.
                termsAndConditions:
                  type: string
                  description: The terms and conditions for the Data Offer.
                offerBudget:
                  type: number
                  description: The budget for the offer.
      responses:
        '204':
          description: OK
        '401':
          description: Unauthorized
      security:
        - BearerAuth: []
components:
  securitySchemes:
    BearerAuth:
      type: http
      scheme: bearer
    ApiKeyAuth:
      type: apiKey
      in: header
      name: X-API-KEY
